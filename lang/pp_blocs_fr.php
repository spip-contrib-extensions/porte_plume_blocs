<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/porte_plume_extras/codes/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'outil_inserer_bloc_info' => "Insérer un bloc d'information",
	'outil_inserer_bloc_success' => "Insérer un bloc de succès",
	'outil_inserer_bloc_warning' => "Insérer un bloc de danger",
	'outil_inserer_bloc_error' => "Insérer un bloc d'erreur",

);

?>
